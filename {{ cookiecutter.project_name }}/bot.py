import logging
import json
import os
from aiogram import Bot, Dispatcher, types

# Logger initialization and logging level setting
log = logging.getLogger(__name__)
log.setLevel(os.environ.get('LOGGING_LEVEL', 'INFO').upper())

TOKEN = os.getenv("TOKEN")


async def process_event(event, dp: Dispatcher):
    """
    Converting an Yandex.Cloud functions event to an update and
    handling tha update.
    """

    try:
        update = json.loads(event['body'])
    except Exception as e:
        log.exception(f"Event is not json: {e}")
    else:
        log.info("Got TG update, started processing")
        log.debug(f"Update: {update}")

        Bot.set_current(dp.bot)
        update = types.Update.to_object(update)
        await dp.process_update(update)


async def handler(event, context):
    """Yandex.Cloud functions handler."""

    bot = Bot(TOKEN)
    dp = Dispatcher(bot)

    @dp.message_handler(commands=['start'])
    async def {{ cookiecutter.bot_function_name }}(message: types.Message):
        """
        Replying to users message, when command /start received
        """
        log.info("Replying to user")
        await message.reply("{{ cookiecutter.reply_text }}")

    await process_event(event, dp)

    return {'statusCode': 200, 'body': 'ok'}